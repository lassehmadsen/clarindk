
<!-- README.md is generated from README.Rmd. Please edit that file -->

# clarindk

<!-- badges: start -->

<!-- badges: end -->

The goal of `clarindk` is to provide access to a clean version of Korpus
2010 from the DK-CLARIN project. This is a general purpose Corpus for
the Danish language, described in more detail here:
<https://korpus.dsl.dk/resources.html>.

## Installation

You can install the development version of clarindk by cloning
`clarindk` from devops and building. Both takes quite some time, because
the repo contains some large files (see below).

## Details

`clarindk` includes functions to read, clean and save the Korpus 2010
corpus from the DK-CLARIN project in one file. It also includes that
cleaned version, so you don’t have to generate it yourself, as this
takes a bit of time. The cleaned corpus is available both as a tokenized
version (with lemmas and POS-tagging), and as a document-level verion.
“Documents” here are sentences or sentence-like bits.

The package also includes word vectors, trained on that corpus. You can
generate all of the above mentioned files again by cloning the project,
downloading the corpus from the above site, (default folder:
`KDK-2010.scrambled/`) and running the script `clean-dkclarin.R` in the
`inst/` folder.

If you just want to *use* those files, simply do:

``` r
load("data/dkkorpus2010.rda")              # Tidy corpus, tokenized
load("data/dkkorpus2010_sentences.rda")    # Tidy corpus, sentence-like bits
load("data/dkkorpus2010_wordvec300.rda")   # Word vectors, dim = 300
```
